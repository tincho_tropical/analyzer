import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Analyzer {

    private static final String CHARSET_NAME = "utf8";
    static final String DEFAULT_TARGET_ELEMENT_ID = "make-everything-ok-button";

    public static void main(String[] args) {
        String originFilePath;
        String sampleFilePath;
        String originalElementId = DEFAULT_TARGET_ELEMENT_ID;

        originFilePath = args[0];
        sampleFilePath = args[1];

        if(args.length == 3) {
            originalElementId = args[2];
        }

        Analyzer analyzer = new Analyzer();

        try {
            analyzer.findElement(originFilePath,
                    sampleFilePath,
                    originalElementId);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Element findElement(String originFilePath, String sampleFilePath, String originalElementId) throws IOException {

        Element elem = findElementsById(new File(originFilePath), originalElementId);

        printElement(elem, "ORIGINAL ELEMENT");

        List<Element> elementsWithSameTag = findElementsWithSameTag(new File(sampleFilePath), elem.tag());

        String attributesFromElem = getAttributesAsOneString(elem);

        Element similarElement = findSimilarElement(attributesFromElem, elementsWithSameTag);

        printElement(similarElement, "CLOSEST ELEMENT");
        return similarElement;
    }

    private List<Element> findElementsWithSameTag(File htmlFile, Tag originTag) throws IOException {
        Document doc = Jsoup.parse(
                htmlFile,
                CHARSET_NAME,
                htmlFile.getAbsolutePath());

        Elements elementsOriginTag = doc.select(originTag.getName());
        List<Element> result = new ArrayList<>();
        for (Element curElement : elementsOriginTag) {
            result.add(curElement);
        }
        return result;
    }

    private Element findSimilarElement(String element, List<Element> toCompare) {

        int minDistance = Integer.MAX_VALUE;
        Element closestElement = null;

        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();

        for (Element compareElement : toCompare) {
            int compareDistance = levenshteinDistance.computeLevenshteinDistance(
                    element,
                    getAttributesAsOneString(compareElement)
            );
            if (compareDistance < minDistance) {
                minDistance = compareDistance;
                closestElement = compareElement;
            }
        }
        return closestElement;
    }

    private String getAttributesAsOneString(Element element) {
        return element.attributes().asList().stream()
                .map(attr -> attr.getKey() + " = " + attr.getValue())
                .collect(Collectors.joining(", "));
    }


    private void printElement(Element element, String elementThatWeAreUsingText) {
        if (element == null) {
            System.out.println(elementThatWeAreUsingText + "null");
        } else {
            System.out.println("PATH TO THE ELEMENT: " + getPathToElement(element));
            System.out.println(elementThatWeAreUsingText + element);
        }
    }

    private String getPathToElement(Element element) {
        String path = "";
        for(int i = element.parents().size()-1; i > 0 ; i--) {
            path = path + element.parents().get(i).tag() + " > ";

        }

        path = path + element.tag();

        return path;
    }


    private Element findElementsById(File htmlFile, String id) {
        Document doc = null;
        try {
            doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return doc.getElementById(id);

    }


    public class LevenshteinDistance {
        private int minimum(int a, int b, int c) {
            return Math.min(Math.min(a, b), c);
        }

        public int computeLevenshteinDistance(CharSequence lhs, CharSequence rhs) {
            int[][] distance = new int[lhs.length() + 1][rhs.length() + 1];

            for (int i = 0; i <= lhs.length(); i++)
                distance[i][0] = i;
            for (int j = 1; j <= rhs.length(); j++)
                distance[0][j] = j;

            for (int i = 1; i <= lhs.length(); i++)
                for (int j = 1; j <= rhs.length(); j++)
                    distance[i][j] = minimum(
                            distance[i - 1][j] + 1,
                            distance[i][j - 1] + 1,
                            distance[i - 1][j - 1] + ((lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1));

            return distance[lhs.length()][rhs.length()];
        }
    }

}