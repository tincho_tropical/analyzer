import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class AnalyzerTest {

    @Test
    public void testSample1() throws IOException {

        Analyzer analyzer = new Analyzer();
        Element element = analyzer.findElement("src/test/resources/sample-0-origin.html",
                "src/test/resources/sample-1-evil-gemini.html",
                Analyzer.DEFAULT_TARGET_ELEMENT_ID);

        Assert.assertEquals("<a class=\"btn btn-success\" href=\"#check-and-ok\" title=\"Make-Button\" rel=\"done\" onclick=\"javascript:window.okDone(); return false;\"> Make everything OK </a>",
                element.toString());

    }

    @Test
    public void testSample2() throws IOException {

        Analyzer analyzer = new Analyzer();
        Element element = analyzer.findElement("src/test/resources/sample-0-origin.html",
                "src/test/resources/sample-2-container-and-clone.html",
                Analyzer.DEFAULT_TARGET_ELEMENT_ID);

        Assert.assertEquals("<a class=\"btn test-link-ok\" href=\"#ok\" title=\"Make-Button\" rel=\"next\" onclick=\"javascript:window.okComplete(); return false;\"> Make everything OK </a>",
                element.toString());

    }

    @Test
    public void testSample3() throws IOException {

        Analyzer analyzer = new Analyzer();
        Element element = analyzer.findElement("src/test/resources/sample-0-origin.html",
                "src/test/resources/sample-3-the-escape.html",
                Analyzer.DEFAULT_TARGET_ELEMENT_ID);

        Assert.assertEquals("<a class=\"btn btn-success\" href=\"#ok\" title=\"Do-Link\" rel=\"next\" onclick=\"javascript:window.okDone(); return false;\"> Do anything perfect </a>",
                element.toString());

    }

    @Test
    public void testSample4() throws IOException {

        Analyzer analyzer = new Analyzer();
        Element element = analyzer.findElement("src/test/resources/sample-0-origin.html",
                "src/test/resources/sample-4-the-mash.html",
                Analyzer.DEFAULT_TARGET_ELEMENT_ID);

        Assert.assertEquals("<a class=\"btn btn-success\" href=\"#ok\" title=\"Make-Button\" rel=\"next\" onclick=\"javascript:window.okFinalize(); return false;\"> Do all GREAT </a>",
                element.toString());

    }


}
