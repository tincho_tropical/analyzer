## The project
Given an element an XML file and an element Id form this file, this project allows you to find the closest element on
another XML file using the Levenshtein algorithm.

## Prerequisites
Java 1.8 or higher

## How to execute:
```
java -jar smart-xml-analyzer.jar <original_html_file> <sample_html_file> [original_html_element_id]
```

